<?php include 'header.php'; ?>
         <!-- Content Start -->
         <div id="main">
            <!-- Title, Breadcrumb Start-->
            <div class="breadcrumb-wrapper">
               <div class="container">
                  <div class="row">
                     <div class="col-lg-6 col-md-6 col-xs-12 col-sm-6">
                        <h2 class="title"><?php echo $this->pageTitle; ?></h2>
                     </div>
                     <div class="col-lg-6 col-md-6 col-xs-12 col-sm-6">
                        <div class="breadcrumbs pull-right">
                        	<?php $this->widget('zii.widgets.CBreadcrumbs', array(
														    'links'=>$this->breadcrumbs,
														)); ?>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Title, Breadcrumb End-->
            <!-- Main Content start-->
            <div class="content">
            <?php echo $content; ?>
            </div>
            <!-- Main Content end-->
         </div>
         <!-- Content End -->
<?php include 'footer.php'; ?>