         <!-- Footer Start -->
         <footer id="footer">
            <!-- Footer Bottom Start -->
            <div class="footer-bottom">
               <div class="container">
                  <div class="row">
                     <div class="col-lg-6 col-md-6 col-xs-12 col-sm-6 "> &copy; All Rights Reserved. <a href="#">Политика конфеденциальности</a>&nbsp;|&nbsp;<a href="#">Права и обязанности</a></div>
                     <div class="col-lg-6 col-md-6 col-xs-12 col-sm-6 ">
                        <ul class="social social-icons-footer-bottom">
                           <li class="facebook"><a href="#" data-toggle="tooltip" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                           <li class="twitter"><a href="#" data-toggle="tooltip" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                           <li class="vkontakte"><a href="#" data-toggle="tooltip" title="ВКонтакте"><i class="fa fa-vk"></i></a></li>
                           <li class="sitemap"><a href="#" data-toggle="tooltip" title="Карта сайта"><i class="fa fa-sitemap"></i></a></li>
                           <li class="contacts"><a href="#" data-toggle="tooltip" title="Контакты"><i class="fa fa-phone"></i></a></li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Footer Bottom End --> 
         </footer>
         <!-- Scroll To Top --> 
         <a href="#" class="scrollup"><i class="fa fa-angle-up"></i></a>
      </div>
      <!-- Wrap End -->
      <!-- The Scripts -->
      <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.min.js"></script>
      <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap.js"></script>
      <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.parallax.js"></script> 
      <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/modernizr-2.6.2.min.js"></script> 
      <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/revolution-slider/js/jquery.themepunch.revolution.min.js"></script>
      <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.nivo.slider.pack.js"></script>
      <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.prettyPhoto.js"></script>
      <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/superfish.js"></script>
      <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/tweetMachine.js"></script>
      <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/tytabs.js"></script>
      <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.gmap.min.js"></script>
      <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/circularnav.js"></script>
      <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.sticky.js"></script>
      <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jflickrfeed.js"></script>
      <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/imagesloaded.pkgd.min.js"></script>
      <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/waypoints.min.js"></script>
      <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/custom.js"></script>
   </body>
</html>