<?php include 'header.php'; ?>
         <!-- Content Start -->
         <div id="main">
            <!-- Slider Start-->
            <div class="fullwidthbanner-container">
               <div class="fullwidthbanner rslider">
                  <ul>
                     <!-- THE FIRST SLIDE -->
                     <li data-delay="6000" data-masterspeed="300" data-transition="fade">
                        <div class="slotholder"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/map-bg.jpg" data-fullwidthcentering="on" alt="Pixma"></div>
                        
                        <div class="caption modern_big_bluebg h1 lft tp-caption start" 
                           data-easing="easeOutExpo" 
                           data-start="0" 
                           data-speed="1050" 
                           data-y="100" 
                           data-x="40">
                           <div class="list-slide">
                              <h3 class="" style="font-size: 20pt; font-weight: 100;"> Создай почтовую открытку, <br>с уникальным стилем и дизайном, <br>которая понравится <br>твоим родным и близким.<br><strong>Всего за 49 <i class="fa fa-rub"></i></strong> </h3>
                           </div>
                        </div>
								<div class="caption lfb caption_button_2 fadeout tp-caption start"
                           data-x="40"
                           data-y="350"
                           data-speed="800"
                           data-endspeed="300"
                           data-start="1000"
                           data-hoffset="70"
                           data-easing="easeOutExpo">
                           <a class="btn-special hidden-xs btn-color" href="/send/" style="font-size: 12pt;">Начать</a>
                        </div>
                     </li>
                  </ul>
               </div>
            </div>
            <!-- Slider End--> 
            <!-- Slogan Start-->
            <div class="slogan bottom-pad-small">
               <div class="container">
                  <div class="row">
                     <div class="slogan-content">
                        <div class="col-lg-9 col-md-9">
                           <h2 class="slogan-title">Открытки в любую точку мира</h2>
                        </div>
                        <div class="col-lg-3 col-md-3">
                           <div class="get-started">
                              <button class="btn btn-special btn-color pull-right" href="/send/">Отправить прямо сейчас!</button>
                           </div>
                        </div>
                        <div class="clearfix"></div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Slogan End--> 
            <!-- Main Content start-->
            <div class="main-content">
               <div class="container">
                  <div class="row">
                     <div class="col-lg-4 col-md-4 col-sm-4 bottom-pad">
                        <div class="content-box ch-item">
                           <div class="ch-info-wrap">
                              <div class="ch-info">
                                 <div class="ch-info-front ch-img-1"></div>
                                 <div class="ch-info-back">
                                 </div>
                              </div>
                           </div>
                           <div class="content-box-info">
                              <h3>Responsive Layout</h3>
                              <p>
                                 Lorem Ipsum is simply dummy text of Lorem the printing and typesettings.
                              </p>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-4 col-md-4 col-sm-4 bottom-pad">
                        <div class="content-box ch-item">
                           <div class="ch-info-wrap">
                              <div class="ch-info">
                                 <div class="ch-info-front ch-img-1"></div>
                                 <div class="ch-info-back">
                                 </div>
                              </div>
                           </div>
                           <div class="content-box-info">
                              <h3>Retina Ready</h3>
                              <p>
                                 Lorem Ipsum is simply dummy text of Lorem the printing and typesettings.
                              </p>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-4 col-md-4 col-sm-4 bottom-pad">
                        <div class="content-box ch-item">
                           <div class="ch-info-wrap">
                              <div class="ch-info">
                                 <div class="ch-info-front ch-img-1"></div>
                                 <div class="ch-info-back">
                                 </div>
                              </div>
                           </div>
                           <div class="content-box-info">
                              <h3>Rich Elements</h3>
                              <p>
                                 Lorem Ipsum is simply dummy text of Lorem the printing and typesettings.
                              </p>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Main Content end--> 
         </div>
         <!-- Content End -->
<?php include 'footer.php'; ?>