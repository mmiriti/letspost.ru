<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->	<html> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<title><?php echo $this->pageTitle; ?></title>
	<meta name="description" content="">
	<!-- Mobile Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Google Fonts -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>
	<!-- Library CSS -->
	<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.css">
	<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/fonts/font-awesome/css/font-awesome.css">
	<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/animations.css" media="screen">
	<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/superfish.css" media="screen">
	<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/revolution-slider/css/settings.css" media="screen">
	<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/prettyPhoto.css" media="screen">
	<!-- Theme CSS -->
	<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.css">
	<!-- Skin -->
	<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/colors/blue.css">
	<!-- Responsive CSS -->
	<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/theme-responsive.css">
	<!-- Favicons -->
	<link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/img/ico/favicon.ico">
	<link rel="apple-touch-icon" href="<?php echo Yii::app()->request->baseUrl; ?>/img/ico/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo Yii::app()->request->baseUrl; ?>/img/ico/apple-touch-icon-72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo Yii::app()->request->baseUrl; ?>/img/ico/apple-touch-icon-114.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo Yii::app()->request->baseUrl; ?>/img/ico/apple-touch-icon-144.png">
   <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
      <!--[if lt IE 9]>
      <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
      <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/respond.min.js"></script>
      <![endif]-->
      <!--[if IE]>
      <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css">
      <![endif]-->
</head>
<body class="home-2">
	<div class="wrap">
		<!-- Modal -->
		<!-- Header Start -->
		<header id="header">
			<!-- Header Top Bar Start -->
			<div class="top-bar">
				<div class="slidedown collapse">
					<div class="container">
						<div class="pull-right">
							<ul class="social pull-left">
								<li class="facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
								<li class="twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
								<li class="vkontakte"><a href="#"><i class="fa fa-vk"></i></a></li>
								<li class="sitemap"><a href="#"><i class="fa fa-sitemap"></i></a></li>
								<li class="contacts"><a href="#"><i class="fa fa-phone"></i></a></li>
							</ul>
							<div id="search-form" class="pull-right">
								<form action="#" method="get">
									<input type="text" class="search-text-box">
								</form>
							</div>
						</div>
						<div class="phone-email pull-right">
							<a href="#" data-toggle="modal" data-target="#userLoginModal">Вход</a> <a>|</a> <?php echo CHtml::link('Регистрация', array('user/registration')); ?>
						</div>
					</div>
				</div>
			</div>
			<!-- Header Top Bar End -->
			<!-- Main Header Start -->
			<div class="main-header">
				<div class="container">
					<!-- TopNav Start -->
					<div class="topnav navbar-header">
						<a class="navbar-toggle down-button" data-toggle="collapse" data-target=".slidedown">
							<i class="fa fa-angle-down fa-current"></i>
						</a> 
					</div>
					<!-- TopNav End -->
					<!-- Logo Start -->
					<div class="logo pull-left">
						<h1>
							<a href="<?php echo Yii::app()->request->baseUrl; ?>/">
								<img src="<?php echo Yii::app()->request->baseUrl; ?>/img/logo.png" alt="pixma" height="60">
							</a>
						</h1>
					</div>
					<!-- Logo End -->
					<!-- Mobile Menu Start -->
					<div class="mobile navbar-header">
						<a class="navbar-toggle" data-toggle="collapse" href=".navbar-collapse">
							<i class="fa fa-bars fa-2x"></i>
						</a> 
					</div>
					<!-- Mobile Menu End -->
					<!-- Menu Start -->
					<nav class="collapse navbar-collapse menu">
						<?php
						$this->widget('zii.widgets.CMenu', array(
							'items'=>array(
								array('label' => 'Главная', 'url' => '/site/index'),    											
								array('label' => 'Отправить открытку', 'url' => '/send'),
								array('label' => 'Новости', 'url' => '/news'),
								array('label' => 'О Нас', 'url' => '/about'),
								array('label' => 'Вопросы и ответы', 'url' => '/faq'),
								),
							'activeCssClass' => 'current',
							'htmlOptions' => array('class' => 'nav navbar-nav sf-menu'),
							'itemCssClass' => 'sf-with-ul'
							)
						);
						?>
					</nav>
					<!-- Menu End --> 
				</div>
			</div>
			<!-- Main Header End -->
		</header>
		<!-- Header End --> 
		<div class="modal fade" id="userLoginModal" tabindex="-1" role="dialog" aria-labelledby="userLoginModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title" id="userLoginModalLabel">Вход</h4>
					</div>
					<div class="modal-body">
						<form class="form-horizontal" role="form">
							<div class="form-group">
								<label for="inputEmail3" class="col-sm-2 control-label">Email</label>
								<div class="col-sm-10">
									<input type="email" class="form-control" id="inputEmail3" placeholder="Email">
								</div>
							</div>
							<div class="form-group">
								<label for="inputPassword3" class="col-sm-2 control-label">Пароль</label>
								<div class="col-sm-10">
									<input type="password" class="form-control" id="inputPassword3" placeholder="Пароль">
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-10">
									<div class="checkbox">
										<label>
											<input type="checkbox"> Запомнить меня
										</label>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-10">
									<a href="#" class="btn btn-primary btn-sm"><i class="fa fa-facebook"></i>&nbsp;&nbsp;Facebook</a>
									<a href="#" class="btn btn-info btn-sm"><i class="fa fa-vk"></i>&nbsp;&nbsp;vkontakte</a>
									<a href="#" class="btn btn-success btn-sm"><i class="fa fa-google-plus"></i>&nbsp;&nbsp;Google+</a>
								</div>
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
						<button type="button" class="btn btn-primary">Войти</button>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
</div><!-- /.modal -->