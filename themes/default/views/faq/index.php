<?php
$this->pageTitle = 'Вопросы и ответы';
?>               
               <div class="container">
                  <div class="row">
                     <div class="posts-block col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <article>
                           <h3 class="title">Вопросы и ответы</h3>
                           <div class="post-content">
                              <p>
                                 There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.
                              </p>
                           </div>
                        </article>
                     </div>
                     <!-- Left Section End -->
                  </div>
                  <div class="divider"></div>
                  <!-- 2 Column Testimonials -->
                  <div class="row">
                     <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
                        <div class="accordionMod panel-group">
                           <div class="accordion-item">
                              <h4 class="accordion-toggle">Question 1</h4>
                              <section class="accordion-inner panel-body">
                                 <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                 </p>
                              </section>
                           </div>
                           <div class="accordion-item">
                              <h4 class="accordion-toggle">Question 2</h4>
                              <section class="accordion-inner panel-body">
                                 <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                 </p>
                              </section>
                           </div>
                           <div class="accordion-item">
                              <h4 class="accordion-toggle">Question 3</h4>
                              <section class="accordion-inner panel-body">
                                 <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                 </p>
                              </section>
                           </div>
                           <div class="accordion-item">
                              <h4 class="accordion-toggle">Question 4</h4>
                              <section class="accordion-inner panel-body">
                                 <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                 </p>
                              </section>
                           </div>
                           <div class="accordion-item">
                              <h4 class="accordion-toggle">Question 5</h4>
                              <section class="accordion-inner panel-body">
                                 <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                 </p>
                              </section>
                           </div>
                           <div class="accordion-item">
                              <h4 class="accordion-toggle">Question 6</h4>
                              <section class="accordion-inner panel-body">
                                 <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                 </p>
                              </section>
                           </div>
                           <div class="accordion-item">
                              <h4 class="accordion-toggle">Question 7</h4>
                              <section class="accordion-inner panel-body">
                                 <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                 </p>
                              </section>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                        <div class="contact-box widget">
                           <h3>More Questions?</h3>
                           <i class="icon-envelope"></i>
                           <p>Let us know your question via the contact form.</p>
                        </div>
                     </div>
                     <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                        <div class="contact-box widget">
                           <h3>Business Hour</h3>
                           <i class="icon-time"> </i>
                           <ul>
                              <li>Monday - Friday 9am to 5pm </li>
                              <li>Saturday - 9am to 2pm</li>
                              <li>Sunday - Closed</li>
                           </ul>
                        </div>
                     </div>
                  </div>
                  <!-- contact box end End-->
               </div>