<?php
$this->pageTitle = 'Отправить открытку';
?>
<input type="file" id="image-file" style="display:none;">

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="jumbotron">
				<h1>Загрузка изображения</h1>
				<p><a class="btn btn-primary btn-lg" role="button" id="image-file-button" onclick="$('#image-file').click();">Выбрать файл</a></p>
			</div>
		</div>
	</div>
</div>