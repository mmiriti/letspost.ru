<?php
$this->pageTitle = 'Регистрация';
?>
<div class="container">
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<form class="form-horizontal" role="form">
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">Имя</label>
					<div class="col-sm-10">
						<input type="email" class="form-control" id="inputEmail3">
					</div>
				</div>
				<div class="form-group">
					<label for="inputPassword3" class="col-sm-2 control-label">Фамилия</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="inputPassword3">
					</div>
				</div>
				<div class="form-group">
					<label for="inputPassword3" class="col-sm-2 control-label">Отчество</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="inputPassword3">
					</div>
				</div>
				<div class="form-group">
					<label for="inputPassword3" class="col-sm-2 control-label">Дата рождения</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="inputPassword3">
					</div>
				</div>
				<div class="form-group">
					<label for="inputPassword3" class="col-sm-2 control-label">Пол</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="inputPassword3">
					</div>
				</div>
				<div class="form-group">
					<label for="inputPassword3" class="col-sm-2 control-label">Email</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="inputPassword3">
					</div>
				</div>
				<div class="form-group">
					<label for="inputPassword3" class="col-sm-2 control-label">Пароль</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="inputPassword3">
					</div>
				</div>
				<div class="form-group">
					<label for="inputPassword3" class="col-sm-2 control-label">Пароль еще раз</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="inputPassword3">
					</div>
				</div>				
				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
						<button type="submit" class="btn btn-primary">Регистрация</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>