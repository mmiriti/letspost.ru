<?php
$this->pageTitle = 'Ошибка '. $code;
?>              
<div class="container">
   <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <h2 class="text-center">Ой... Ошибка !!!</h2>
         <div id="not-found">
            <h2><?php echo $code; ?> <i class="icon-question-sign"></i></h2>
         </div>
         <div class="back-home">
            <p><?php echo CHtml::encode($message); ?></p>
         </div>
      </div>
   </div>
</div>