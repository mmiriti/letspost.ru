<?php
$this->pageTitle = 'Новости';
?>                
               <div class="container">
                  <div class="row">
                     <div class="posts-block col-lg-9 col-md-9 col-sm-8 col-xs-12">
                        <article class="post hentry">
                           <div class="post-image">
                              <a href="img/blog/blog-post1.jpg" data-rel="prettyPhoto">
                              <span class="img-hover"></span>
                              <span class="fullscreen"><i class="icon-search"></i></span>
                              <img src="img/blog/blog-post1.jpg" alt="">
                              </a>
                           </div>
                           <div class="post-content-wrap">
                              <header class="post-header">
                                 <div class="post-date">
                                    <a class="date" href="#"><strong>18</strong><i>SEP</i></a>
                                 </div>
                                 <h3 class="content-title">Blog post title</h3>
                                 <div class="blog-entry-meta">
                                    <div class="blog-entry-meta-author">
                                       <i class="icon-user"></i>
                                       <a href="#" class="blog-entry-meta-author">John Doe</a>
                                    </div>
                                    <div class="blog-entry-meta-tags">
                                       <i class="icon-tags"></i>
                                       <a href="#">Web Design</a>,
                                       <a href="#">Branding</a>
                                    </div>
                                    <div class="blog-entry-meta-comments">
                                       <i class="icon-comments"></i>
                                       <a href="#" class="blog-entry-meta-comments">4 comments</a>
                                    </div>
                                 </div>
                              </header>
                              <div class="post-content">
                                 <p>
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen not only five centuries, but also the leap essentially.
                                 </p>
                              </div>
                           </div>
                           <div class="clearfix"></div>
                        </article>
                        <div class="blog-divider"></div>
                        <article class="post hentry">
                           <div class="post-image">
                              <a href="img/blog/blog-post1.jpg" data-rel="prettyPhoto">
                              <span class="img-hover"></span>
                              <span class="fullscreen"><i class="icon-search"></i></span>
                              <img src="img/blog/blog-post1.jpg" alt="">
                              </a>
                           </div>
                           <div class="post-content-wrap">
                              <header class="post-header">
                                 <div class="post-date">
                                    <a class="date" href="#"><strong>12</strong><i>SEP</i></a>
                                 </div>
                                 <h3 class="content-title">Blog post title</h3>
                                 <div class="blog-entry-meta">
                                    <div class="blog-entry-meta-author">
                                       <i class="icon-user"></i>
                                       <a href="#" class="blog-entry-meta-author">John Doe</a>
                                    </div>
                                    <div class="blog-entry-meta-tags">
                                       <i class="icon-tags"></i>
                                       <a href="#">Web Design</a>,
                                       <a href="#">Branding</a>
                                    </div>
                                    <div class="blog-entry-meta-comments">
                                       <i class="icon-comments"></i>
                                       <a href="#" class="blog-entry-meta-comments">4 comments</a>
                                    </div>
                                 </div>
                              </header>
                              <div class="post-content">
                                 <p>
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen not only five centuries, but also the leap essentially.
                                 </p>
                              </div>
                           </div>
                           <div class="clearfix"></div>
                        </article>
                        <div class="blog-divider"></div>
                        <article class="post hentry">
                           <div class="post-image">
                              <a href="img/blog/blog-post1.jpg" data-rel="prettyPhoto">
                              <span class="img-hover"></span>
                              <span class="fullscreen"><i class="icon-search"></i></span>
                              <img src="img/blog/blog-post1.jpg" alt="">
                              </a>
                           </div>
                           <div class="post-content-wrap">
                              <header class="post-header">
                                 <div class="post-date">
                                    <a class="date" href="#"><strong>05</strong><i>SEP</i></a>
                                 </div>
                                 <h3 class="content-title">Blog post title</h3>
                                 <div class="blog-entry-meta">
                                    <div class="blog-entry-meta-author">
                                       <i class="icon-user"></i>
                                       <a href="#" class="blog-entry-meta-author">John Doe</a>
                                    </div>
                                    <div class="blog-entry-meta-tags">
                                       <i class="icon-tags"></i>
                                       <a href="#">Web Design</a>,
                                       <a href="#">Branding</a>
                                    </div>
                                    <div class="blog-entry-meta-comments">
                                       <i class="icon-comments"></i>
                                       <a href="#" class="blog-entry-meta-comments">4 comments</a>
                                    </div>
                                 </div>
                              </header>
                              <div class="post-content">
                                 <p>
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen not only five centuries, but also the leap essentially.
                                 </p>
                              </div>
                           </div>
                           <div class="clearfix"></div>
                        </article>
                        <div class="blog-divider"></div>
                        <article class="post hentry">
                           <div class="post-image">
                              <a href="img/blog/blog-post1.jpg" data-rel="prettyPhoto">
                              <span class="img-hover"></span>
                              <span class="fullscreen"><i class="icon-search"></i></span>
                              <img src="img/blog/blog-post1.jpg" alt="">
                              </a>
                           </div>
                           <div class="post-content-wrap">
                              <header class="post-header">
                                 <div class="post-date">
                                    <a class="date" href="#"><strong>27</strong><i>AUG</i></a>
                                 </div>
                                 <h3 class="content-title">Blog post title</h3>
                                 <div class="blog-entry-meta">
                                    <div class="blog-entry-meta-author">
                                       <i class="icon-user"></i>
                                       <a href="#" class="blog-entry-meta-author">John Doe</a>
                                    </div>
                                    <div class="blog-entry-meta-tags">
                                       <i class="icon-tags"></i>
                                       <a href="#">Web Design</a>,
                                       <a href="#">Branding</a>
                                    </div>
                                    <div class="blog-entry-meta-comments">
                                       <i class="icon-comments"></i>
                                       <a href="#" class="blog-entry-meta-comments">4 comments</a>
                                    </div>
                                 </div>
                              </header>
                              <div class="post-content">
                                 <p>
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen not only five centuries, but also the leap essentially.
                                 </p>
                              </div>
                           </div>
                           <div class="clearfix"></div>
                        </article>
                        <div class="blog-divider"></div>
                        <article class="post hentry">
                           <div class="post-image">
                              <a href="img/blog/blog-post1.jpg" data-rel="prettyPhoto">
                              <span class="img-hover"></span>
                              <span class="fullscreen"><i class="icon-search"></i></span>
                              <img src="img/blog/blog-post1.jpg" alt="">
                              </a>
                           </div>
                           <div class="post-content-wrap">
                              <header class="post-header">
                                 <div class="post-date">
                                    <a class="date" href="#"><strong>10</strong><i>AUG</i></a>
                                 </div>
                                 <h3 class="content-title">Blog post title</h3>
                                 <div class="blog-entry-meta">
                                    <div class="blog-entry-meta-author">
                                       <i class="icon-user"></i>
                                       <a href="#" class="blog-entry-meta-author">John Doe</a>
                                    </div>
                                    <div class="blog-entry-meta-tags">
                                       <i class="icon-tags"></i>
                                       <a href="#">Web Design</a>,
                                       <a href="#">Branding</a>
                                    </div>
                                    <div class="blog-entry-meta-comments">
                                       <i class="icon-comments"></i>
                                       <a href="#" class="blog-entry-meta-comments">4 comments</a>
                                    </div>
                                 </div>
                              </header>
                              <div class="post-content">
                                 <p>
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen not only five centuries, but also the leap essentially.
                                 </p>
                              </div>
                           </div>
                           <div class="clearfix"></div>
                        </article>
                        <div class="blog-divider"></div>
                        <div class="pagination-centered">
                           <ul class="pagination">
                              <li class="disabled"><a href="#">«</a></li>
                              <li class="active"><a href="#">1</a></li>
                              <li><a href="#">2</a></li>
                              <li><a href="#">3</a></li>
                              <li><a href="#">4</a></li>
                              <li><a href="#">5</a></li>
                              <li><a href="#">»</a></li>
                           </ul>
                        </div>
                     </div>
                     <div class="sidebar col-lg-3 col-md-3 col-sm-4 col-xs-12">
                        <!-- Search Widget Start -->
                        <div class="widget search-form">
                           <div class="input-group">
                              <input type="text" value="Search..." onfocus="if(this.value=='Search...')this.value='';" onblur="if(this.value=='')this.value='Search...';" class="search-input form-control">
                              <span class="input-group-btn">
                              <button type="submit" class="subscribe-btn btn"><i class="icon-search"></i></button>
                              </span>
                           </div>
                           <!-- /input-group -->
                        </div>
                        <!-- Search Widget End -->
                        <!-- Tab Start -->
                        <div class="widget tabs">
                           <div id="horizontal-tabs">
                              <ul class="tabs">
                                 <li id="tab1" class="current">Popular</li>
                                 <li id="tab2">Recent</li>
                                 <li id="tab3">Comments</li>
                              </ul>
                              <div class="contents">
                                 <div class="tabscontent" id="content1" style="display: block;">
                                    <ul class="posts">
                                       <li>
                                          <a href="#"><img class="img-thumbnail recent-post-img" alt="" src="img/recent-post-img.jpg"></a>
                                          <p><a href="#">Lorem Ipsum is simply dummy text.</a></p>
                                          <span class="color">27 July 2013</span>
                                       </li>
                                       <li>
                                          <a href="#"><img class="img-thumbnail recent-post-img" alt="" src="img/recent-post-img.jpg"></a>
                                          <p><a href="#">Lorem Ipsum is simply dummy text.</a></p>
                                          <span class="color">30 July 2013</span>
                                       </li>
                                    </ul>
                                 </div>
                                 <div class="tabscontent" id="content2">
                                    <ul class="posts">
                                       <li>
                                          <a href="#"><img class="img-thumbnail recent-post-img" alt="" src="img/recent-post-img.jpg"></a>
                                          <p><a href="#">Lorem Ipsum is simply dummy text.</a></p>
                                          <span class="color">27 July 2013</span>
                                       </li>
                                       <li>
                                          <a href="#"><img class="img-thumbnail recent-post-img" alt="" src="img/recent-post-img.jpg"></a>
                                          <p><a href="#">Lorem Ipsum is simply dummy text.</a></p>
                                          <span class="color">30 July 2013</span>
                                       </li>
                                    </ul>
                                 </div>
                                 <div class="tabscontent" id="content3">
                                    <ul class="posts">
                                       <li>
                                          <a href="#"><img class="img-thumbnail recent-post-img" alt="" src="img/recent-post-img.jpg"></a>
                                          <p><a href="#">Lorem Ipsum is simply dummy text.</a></p>
                                          by <span class="color">wptuts+</span>
                                       </li>
                                       <li>
                                          <a href="#"><img class="img-thumbnail recent-post-img" alt="" src="img/recent-post-img.jpg"></a>
                                          <p><a href="#">Lorem Ipsum is simply dummy text.</a></p>
                                          by <span class="color">wptuts+</span>
                                       </li>
                                    </ul>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- Tab End -->
                     </div>
                     <!-- Sidebar End -->     
                  </div>
               </div>