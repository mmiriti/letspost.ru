<?php
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Let\'s Post',
	'theme'=>'default',
	'language'=>'ru',
	'preload'=>array('log'),
	'import'=>array(
		'application.models.*',
		'application.components.*',
		),

	'modules'=>array(
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'gii',
			'ipFilters'=>array('127.0.0.1','::1'),
			),
		),
	'components'=>array(
		'user'=>array(
			'allowAutoLogin'=>true,
			),
		'urlManager'=>array(
			'urlFormat'=>'path',
			'showScriptName'=>false,
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
				),
			),
		'db'=> include 'db.php',
		'errorHandler'=>array(
			'errorAction'=>'error/index',
			),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
					),
				),
			),
		),
	'params'=>array(
		'adminEmail'=>'webmaster@example.com',
		),
	);